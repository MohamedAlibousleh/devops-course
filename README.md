# Devops Course - Guidance

This repository supports the devops intro course at xxllnc.

There are two seperate courses:

1. Kubernetes and Helm
2. Terraform

# Content of this repository

## frontend and backend

A demonstration of Docker to implement a simple 3 tier architecture

* frontend will be able to access the mid-tier
* mid-tier will be able to access the db

In order to run this in docker, simply type ```docker-compose up``` at the command prompt. Docker will then create the [MongoDB](https://www.mongodb.com/) from the stock [mongo](https://hub.docker.com/_/mongo) image. The api uses [nodejs](https://nodejs.org/) with [express](http://expressjs.com/) and is built from a [node:alpine](https://hub.docker.com/_/node) image. The front end uses [ReactJS](https://reactjs.org/) and built from a [node:alpine](https://hub.docker.com/_/node) image.

# Karma points

Thank you knaopel (https://github.com/knaopel/docker-frontend-backend-db) for this simple todo list manager which actually works